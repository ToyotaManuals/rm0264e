#!/usr/bin/env python3
from http.server import HTTPServer, SimpleHTTPRequestHandler, test
import sys
import os

class CORSRequestHandler (SimpleHTTPRequestHandler):
    def end_headers (self):
        path = self.translate_path(self.path)
        if os.path.splitext(path)[1] == '.svgz':
            self.send_header("Content-Encoding", 'gzip')
        SimpleHTTPRequestHandler.end_headers(self)

test(CORSRequestHandler, HTTPServer, port=int(sys.argv[1]) if len(sys.argv) > 1 else 8000)
